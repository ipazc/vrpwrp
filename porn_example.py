#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time

from helpers import image_helper
from wrappers.porn_classification import PornClassification

__author__ = 'Iván de Paz Centeno'

start_time = time.time()

porn_classifier = PornClassification()

imagen_bytes1 = image_helper.get_file_binary_content("Fronalpstock_big.jpg")

print(porn_classifier.is_porn(imagen_bytes1))
end_time = time.time()
print("Time: {}s".format(end_time - start_time))