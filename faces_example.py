#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time

from helpers import image_helper
from wrappers.face_detection import FaceDetection
from wrappers.face_recognition import FaceRecognition

start_time = time.time()
face_detection = FaceDetection()
face_recognition = FaceRecognition()


#1. Cargamos en memoria las imágenes que queremos analizar
imagen_bytes1 = image_helper.get_file_binary_content("examples/subject1_1.jpg")
imagen_bytes2 = image_helper.get_file_binary_content("examples/subject1_2.jpg")



#2. Obtenemos las posiciones de las caras
bounding_boxes1 = face_detection.analyze(imagen_bytes1)
bounding_boxes2 = face_detection.analyze(imagen_bytes2)

# Los bounding boxes representan los recuadros. Las imágenes de prueba solamente tienen uno, asi que sacamos el primero
bb1 = bounding_boxes1[0]
bb2 = bounding_boxes2[0]

print("Cara de la imagen 1: ", bb1)
print("Cara de la imagen 2: ", bb2)



#3. Recortamos las caras de las imágenes.
## Necesitamos cargar los bytes en una imagen PIL para poder recortar
imagen1 = image_helper.get_image(imagen_bytes1)
imagen2 = image_helper.get_image(imagen_bytes2)

## Ahora podemos recortar
recortado1 = image_helper.crop_by_bbox(imagen1, bb1)
recortado2 = image_helper.crop_by_bbox(imagen2, bb2)

# Recortado1 y recortado2 son imágenes PIL. Se pueden guardar o visualizar:
recortado1.show()
recortado2.show()



#4. Extraemos la identidad de los rostros detectados.
identidad_imagen1 = face_recognition.get_embedding(image_helper.to_byte_array(recortado1))
identidad_imagen2 = face_recognition.get_embedding(image_helper.to_byte_array(recortado2))

# Las identidades se pueden almacenar en una base de datos. Son números.
print(identidad_imagen1)
print(identidad_imagen2)


#5. Comparamos dos identidades para ver cuánto se parecen
print("Los rostros pertenecen al mismo si su resta es menor que 1: ", identidad_imagen1 - identidad_imagen2)
end_time = time.time()

print("Time took: {}s".format(end_time-start_time))
